import requests


URL = "http://127.0.0.1:5000/{}"


def create_posts():

    for i in range(100):
        username = "mod10" if i % 10 == 0 else "__NonDecimal__"
        requests.post(URL.format("posts"), json={
            "title": f"My {i} Post!",
            "body": "How exciting!",
            "username": username
        })


def fetch_posts():
    posts = requests.get(URL.format("posts"), params={
        "limit": 2
    }).json()

    more_posts = requests.get(URL.format("posts"), params={
        "limit": 2,
        "before": posts["results"][-1]["creation_time"]
    }).json()

    return posts["results"], more_posts["results"]


def posts_number():
    return requests.get(URL.format("postsnumber")).json()["posts_number"]


def get_top_creators():
    return requests.get(URL.format("statistics/topcreators")).json()["results"]


def get_runtimes():
    return requests.get(URL.format("statistics/runtimes")).json()["results"]


def run_examples():
    print("Creating posts")
    create_posts()
    print("There are now", posts_number(), "posts!")

    posts, more_posts = fetch_posts()
    print(posts)
    print(more_posts)

    print("The top creators are:")
    print(get_top_creators())

    print("And all this took on average:")
    print(get_runtimes())


if __name__ == "__main__":
    run_examples()

