from datetime import datetime
from functools import wraps

from flask import Blueprint, request

from orm import db, Post, User, Runtime, Function

routes = Blueprint("", __name__)


def profiled(func):
    """
    Decorator that saves the decorated function's runtime in the database.
    todo: Write to logs instead, logstash the logs to an elastic server, use kibana to show pretty graphs, PROFIT.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = datetime.now()
        result = func(*args, **kwargs)
        run_time = (datetime.now() - start_time).total_seconds()

        function = Function.get_or_create(name=f"{func.__module__}.{func.__name__}")
        runtime = Runtime(run_date=start_time, runtime=run_time, function=function)
        db.session.add(runtime)
        db.session.commit()

        return result

    return wrapper


@routes.route("/ping")
def ping():
    return "pong"


@routes.route("/posts", methods=['POST'])
@profiled
def create_post():
    """
     Create a new post. The request should be in json format with the following fields:
     title: String - the post title.
     body: String - the post body.
     username: String - the post creator's username, the user will be created if it does not exist.
    """
    json = request.json

    post = Post(title=json['title'], body=json['body'], user=User.get_or_create(username=json['username']))
    db.session.add(post)
    db.session.commit()

    return ""


@routes.route("/posts")
@profiled
def get_posts():
    """
    Get a chunk of posts in order - newest first.
    Arguments:
    before: float - Only get posts before this timestamp, default - now()
    limit: int - Return a maximum of this amount of posts, default - 10
    """
    before_date = float(request.args.get("before", datetime.now().timestamp()))
    post_count = request.args.get("limit", 10)

    before_date = datetime.fromtimestamp(before_date)

    posts = Post.query.\
        filter(Post.creation_time < before_date).\
        order_by(Post.creation_time.desc()).\
        limit(post_count)

    return {
        "results": [post.to_dict() for post in posts]
    }


@routes.route("/postsnumber")
def posts_number():
    """
    Return the number of posts in the database.
    """

    return {
        "posts_number": Post.query.count()
    }
