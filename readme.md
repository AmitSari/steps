# README #

Hey, this is my mini-server, I hope you like it!



### How to run: ###

* Install the requirements with 'pip install -r requirements.txt'
* Start a postgres database with the user and password: postgres
  * Run the file orm.py to DELETE all tables and recreate them.
  * Alternatively set the STEPS_SERVER_URI environment variable to 'sqlite://' to run against an in memory sqlite server instead.
* Start the server with 'flask run' or just run app.py
* Send requests to 127.0.0.1:5000
* There is also an example_api_calls.py file with example requests written.
