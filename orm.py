from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class GetOrCreateTable(db.Model):
    __abstract__ = True

    @classmethod
    def get_or_create(cls, **kwargs):
        exists = cls.query.filter_by(**kwargs).one_or_none()
        if exists is None:
            exists = cls(**kwargs)
        return exists


class Post(db.Model):
    __tablename__ = "posts"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    body = db.Column(db.String)

    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    user = db.relationship('User')

    creation_time = db.Column(db.DateTime, default=datetime.now)

    def to_dict(self):
        return {
            "id": self.id,
            "title": self.title,
            "body": self.body,
            "user_id": self.user_id,
            "creation_time": self.creation_time.timestamp()
        }


class User(GetOrCreateTable):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, unique=True)


class Runtime(db.Model):
    __tablename__ = "runtimes"

    id = db.Column(db.Integer, primary_key=True)

    run_date = db.Column(db.DateTime)
    runtime = db.Column(db.Float, nullable=False)

    function_id = db.Column(db.Integer, db.ForeignKey("functions.id"), nullable=False)
    function = db.relationship('Function')


class Function(GetOrCreateTable):
    __tablename__ = "functions"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)


if __name__ == "__main__":
    from app import create_app
    app = create_app()
    with app.app_context():
        db.drop_all()
        db.create_all()
