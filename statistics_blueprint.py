from flask import Blueprint, request

from orm import db, Post, Runtime, Function
from sqlalchemy import func

statistics = Blueprint("statistics", __name__)


@statistics.route("/topcreators")
def topcreators():
    """
    Get the users with the most posts.
    Returns a list of the user_ids and their post count.
    In case of a tie the user who posted later wins.
    """
    limit_count = request.args.get("count", 10)

    amount_field = func.count().label("amount")

    results = db.session.query(Post.user_id, amount_field).\
        group_by(Post.user_id).\
        order_by(amount_field.desc(), func.max(Post.creation_time).desc()).\
        limit(limit_count)

    return {
        "results": [
            {
                "user_id": row.user_id,
                "post_count": row.amount
            } for row in results
        ]
    }


@statistics.route("/runtimes")
def runtimes():
    """
    Get the average runtimes of all profiled functions.
    """
    results = db.session.query(Function.name.label("function_name"), func.avg(Runtime.runtime).label("average_runtime")).\
        join(Runtime).\
        group_by(Function.name).\
        all()
    return {
        "results": [
            {
                "function": row.function_name,
                "average_runtime": row.average_runtime
            } for row in results
        ]
    }
