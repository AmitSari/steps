import os

from flask import Flask
from orm import db
from routes import routes
from statistics_blueprint import statistics


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get('STEPS_SERVER_URI',
                                                           "postgresql://postgres:postgres@127.0.0.1")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["SQLALCHEMY_ECHO"] = True

    db.init_app(app)

    with app.app_context():
        db.create_all()

    app.register_blueprint(routes)
    app.register_blueprint(statistics, url_prefix="/statistics")
    return app


if __name__ == "__main__":
    application = create_app()
    application.run()

